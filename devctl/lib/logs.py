# -*- coding: utf-8 -*-
"""Development Control Tool - logging handling.

This file is responsible for setting up loggers in the devctl application.

Usage:
    from devctl.lib.logs import LogConfig

    log = LogConfig()

Todo:
    * Add docstrings

"""
import logging
