# -*- coding: utf-8 -*-
"""Development Control Tool.

This tool is intended to ease daily developer's work. It can be adjusted to any
environment by writing and connecting modules - each serving different purpose.

Example:
    Starting module with options.

        $ python devctl.py module_name --module-option option

Attributes:
    BASE_DIR (str): Main directory where all application binaries, libraries,
        modules and configuration files are stored.
    MODULES_JSON (str): File name of the modules JSON configuration.

Todo:
    * Add docstrings

"""
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(BASE_DIR))

MODULES_FILENAME = 'modules.json'


def main():
    """Main function of the devctl application."""
    print("base: {}".format(BASE_DIR))
    print("path: {}".format(sys.path))


if __name__ == "__main__":
    try:
        main()
    except Exception as exc:
        raise
