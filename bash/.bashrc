#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Bash options
shopt -s checkwinsize

# Enable Bash completion
source /usr/share/bash-completion/bash_completion

# Enable git prompt features.
source /usr/share/git/git-prompt.sh

# Colors
BLUE=$(tput setaf 33)
GREEN=$(tput setaf 65)
PURPLE=$(tput setaf 135)
RED=$(tput setaf 9)
YELLOW=$(tput setaf 220)
RESET=$(tput sgr0)

# Aliases
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ll='ls -al --color=auto'
alias ls='ls -a --color=auto'

# Environmental additions
export EDITOR=vim
# export PATH="$PATH:/new/path"

# Functions

# Prints exit code of the command with appropriate color.
exit_code() {
  local code=$?
  if [[ $code == 0 ]]; then
    printf "\001${GREEN}\002${code}\001${RESET}\002"
  else
    printf "\001${RED}\002${code}\001${RESET}\002"
  fi
}

# Print git-related information.
git_info() {
  printf "\001${GREEN}\002$(__git_ps1 '(%s)')\001${RESET}\002"
}

# Print timestamp.
timestamp() {
  printf "\001${PURPLE}\002$(date '+%H:%M:%S')\001${RESET}\002"
}

# Command prompt
PS1='[$(exit_code)] [$(timestamp)] \[${BLUE}\][\u@\h \[${YELLOW}\]\W\[${BLUE}\]]\[${RESET}\] $(git_info) \$ '
