# devutils

Utility scripts and applications useful for code development in Linux
environment.

List of things in repository:
 - customized .bashrc
 - devctl utility application

# Installation

## .bashrc
To use .bashrc from this repository:

1. Backup existing .bashrc:
    ```
    mv ~/.bashrc ~/.bashrc.old
    ```
2. Copy .bashrc into your home directory:
    ```
    cp bash/.bashrc ~/
    ```
3. Modify ~/.bashrc according to your personal preferences.

## devctl

# Usage

# Contributing

## Development environment setup
If you're interested in devutils development you need to setup you environment
with following instructions:

1. Create virtual Python environment:
    ```
    virtualenv -p /path/to/python/executable devutils
    ```
2. Enter created virtual environment:
    ```
    source devutils/bin/activate
    ```
3. Install required dependencies:
    ```
    pip install -r requirements.txt
    ```

## Delivering changes
Once you have your setup ready and you've made some changes you can deliver them
onto your personal branch.

1. If you don't have personal branch created create it:
    ```
    git checkout -b personal/foo
    ```
2. Stage the changes and make a commit:
    ```
    git add -u          # or git add .*
    git commit -m "your commit message"
    ```
3. Push the changes on the remote:
    ```
    git push origin personal/foo
    ```
4. Create pull request.

# Changelog

- 0.0.1
    - Created basic project structure.
    - Added .bashrc example.

# Meta

Szczepan Smoczek - thecore.p3@gmail.com

Software is being distributed under the MIT license. See ```LICENSE``` for more
information.

https://bitbucket.org/szagix/
